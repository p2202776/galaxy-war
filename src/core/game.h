#ifndef GAME_H
#define GAME_H

#include <vector>
#include "ennemi.h"
#include "avion.h"
#include "map.h"
#include "config.h"


class Game{
public:

      Ennemi e ;
      Avion avion;
      Ennemi tabEnnemi[4];
      Map M;



    Game();
    void gestionClavier(char c,const Point & point) ;
    void gestionClavier(char c) ;
    bool  verifieCollision(Rectangle rect1 ,Point balle);
    void collisionAvionBalle();
    void gameOver();

    void initTabEnnemi();
  //inline const std::vector<Ennemi>   & getConstEnnemi() const{return tab;}
  void UpdateEnnemi();

};

#endif