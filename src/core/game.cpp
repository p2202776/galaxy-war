
#include "game.h"






Game::Game() :e(),avion(10,6,1,20) , M(){


    for (int i=0 ;i<4 ;i++){
        tabEnnemi[i].pos.posX =40;
        tabEnnemi[i].pos.posY=(i+1)*2 ;
        //tabEnnemi[i].tabBalle[i].posBalle = {tabEnnemi[i].pos.posX, tabEnnemi[i].pos.posY};

    }
}


void Game::initTabEnnemi() {
    for (int i=0 ;i<4 ;i++){
        tabEnnemi[i].pos.posX =40;
        tabEnnemi[i].pos.posY=(i+1)*2 ;
        //tabEnnemi[i].tabBalle[i].posBalle = {tabEnnemi[i].pos.posX, tabEnnemi[i].pos.posY};

    }
}

bool Game::verifieCollision(Rectangle rect1 ,Point balle) {
    return ((balle.posX>=rect1.x && balle.posX<rect1.x+rect1.w)&&(balle.posY>=rect1.y && balle.posY<rect1.y+rect1.h)) ;
}

void Game::collisionAvionBalle() {
    Rectangle rectAvion = { (int)avion.getPosX(), (int)avion.getPosY(), avionWidth, avionHeigh };

    // Parcourir toutes les balles ennemies
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < tabEnnemi[i].tabBalle.size(); j++) {
            Point pos= {tabEnnemi[i].tabBalle[j].posBalle.posX,tabEnnemi[i].tabBalle[j].posBalle.posY};
            if (verifieCollision(rectAvion, pos)) {


                avion.setVie(avion.getVie()- 1);
            }
        }
    }

}

void Game::gameOver() {
    if(avion.getVie()==0){
        avion.setPosX(-100);
        avion.setPosY(-100);
    }
}

void Game::gestionClavier(char c, const Point & point) {
    switch (c) {
        case 'l':
            avion.bougerAvionGauche(point);
            break;
        case 'r':
            avion.bougerAvionDroite(point);
            break;
        case 'u':
            avion.bougerAvionHaut(point);
            break;
        case 'd':
            avion.bougerAvionBas(point);
            break;
        case 'x': avion.tirer();
            break;
    }
}
