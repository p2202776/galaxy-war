#include "avion.h"
#include <cassert>
#include <iostream>


int  Avion::getPosX() const {
    return this->position.posX;
}
int  Avion::getPosY() const{
    return  this->position.posY;
}

std::vector<Balle> Avion::getTab() const {return this->tab;}

int Avion::getVie() {
    return vie ;
}

void Avion::setVie(int _vie) {
     this->vie =_vie;
}

void Avion::setPosX(int x) {
    this->position.posX =x ;
}

void Avion::setPosY(int y) {
    this->position.posY =y;
}


Avion::Avion()
{
    this->position.posX=0;
    this->position.posY=0;
    this->vitesse = 0;
    this->vie = 1;
    this->dir = Direction::HAUT;
}
Avion::Avion(int x, int y, float vitesse, int vie)
{
    this->position.posX = x;
    this->position.posY = y;
    this->vitesse = vitesse;
    this->vie = vie;
    this->dir = Direction::HAUT;
}

Avion::~Avion()
{
    this->vie=0;
    this->position.posX = -10;
    this->position.posY = -10;
}

void Avion::bougerAvionDroite(const Point & rectangle)
{
    if(this->position.posX < rectangle.posX-1)
    {
        this->position.posX+=vitesse;

    } else
        this->position.posX = position.posX;
    this->dir = Direction::DROITE;
}

void  Avion::bougerAvionGauche(const Point & rectangle)
{
    if(this->position.posX > 1)
    {
        this->position.posX-=vitesse;

    } else
        this->position.posX = position.posX;
    this->dir = Direction::GAUCHE;
}

void Avion::bougerAvionHaut(const Point & rectangle)
{
    if(this->position.posY > 1)
    {
        this->position.posY-=vitesse;

    }else
        this->position.posY = position.posY;
    this->dir = Direction::HAUT;
}

void Avion::bougerAvionBas(const Point & rectangle)
{
    if(this->position.posY >= rectangle.posY-1)
    {
        this->position.posY = position.posY;
    }
    else
    {
        this->position.posY+=vitesse;

    }
    this->dir = Direction::BAS;
}

void Avion::tirer() {

    Balle b = {this->position.posX,this->position.posY};
    this->tab.push_back(b);
    this->tab[tab.size()-1].d = this->dir;
}

void  Avion::misAjourBalle()
{
    for(int i=0; i<tab.size();i++)
    {
        switch (this->tab[i].d)
        {
            case Direction::HAUT : this->tab[i].posBalle.posY --;
                break;
            case Direction::BAS: this->tab[i].posBalle.posY ++;
                break;
            case Direction::DROITE: this->tab[i].posBalle.posX ++;
                break;
            case Direction::GAUCHE: this->tab[i].posBalle.posX --;
                break;

        }
    }

}

void  Avion::testRegression()
{
    Avion A;
    Point test(50,50);
    assert(A.position.posX == 0);
    assert(A.position.posY == 0);
    A.bougerAvionDroite(test);
    assert((A.position.posX-A.vitesse)==0);

    A.bougerAvionHaut(test);
    assert((A.position.posY-A.vitesse) ==0);

    A.bougerAvionGauche(test);
    assert((A.position.posX-A.vitesse) == 0);

    std::cout << "------------ test avion reussi ------------ \n";

}