#ifndef PROJETCONCEPTION_BALLE_H
#define PROJETCONCEPTION_BALLE_H
#include "rectangle.h"
#include "point.h"
#include "direction.h"
class Balle{
    public:
    Point posBalle ;
    Direction d;
    Balle(int x,int y){

        posBalle.posX = x; // Initialise la position X de la balle avec la valeur passée en paramètre
        posBalle.posY = y; // Initialise la position Y de la balle avec la valeur passée en paramètre

    };

};

#endif