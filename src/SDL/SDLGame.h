#include "SDL.h"
#include "SDLSprite.h"
#include "../CORPS/game.h"


#ifndef SDLGAME_H
#define SDLGAME_H

class SDLGame{
public:
    SDL_Window* fenetre = nullptr ;
    SDL_Renderer *renderer = nullptr;
    //SDL_Texture *texture = nullptr;

    float temps ();

    Game game ;
    SDLSprite sprite ;
    SDLSprite spriteEnnemi ;
    SDLSprite sprite1;
    SDLSprite balleSprite ;
    SDLSprite spriteAvion ;




    SDLGame();
    ~SDLGame();
    void sdlDraw();
    void Renderer();
    void initTabEnnemiTexture(Ennemi tab[]);
    void bougerTabEnnemi(Ennemi tab[]);
    void initBalleTexture();

    void update();

    void runProject();

};

inline float SDLGame::temps() {
    return (float)SDL_GetTicks()/CLOCKS_PER_SEC;
}

#endif