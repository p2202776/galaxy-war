#include "ennemi.h"
#include <iostream>


Ennemi::Ennemi():vie(20) ,tempsDernierTir(0.0){
    pos.posX= 250;
    pos.posY=300;
    vitesse=1 ;
    vitesseBalle=5;
}

void Ennemi::bougerVerticale() {
    this->pos.posY-=vitesse ;
}

void Ennemi::bougerHorizontal() {
    this->pos.posX-=vitesse ;
}


void Ennemi::tirerBalle(float temps) {
   // Uint32 tempsActuel = SDL_GetTicks(); // Obtient le temps actuel en millisecondes

    // Vérifie si suffisamment de temps s'est écoulé depuis le dernier tir (par exemple, 1000 millisecondes)
    if (temps- tempsDernierTir >= 1) {
        creerBalle();
        tempsDernierTir = temps; // Met à jour le temps du dernier tir
    }
}

void Ennemi::creerBalle() {
    // Utiliser la position stockée pour initialiser la balle


    Balle balle(pos.posX, pos.posY);
    tabBalle.push_back(balle);

}

void Ennemi::deplacerBalle() {
    for (int i = 0; i < tabBalle.size(); i++) {
        tabBalle[i].posBalle.posX -= vitesseBalle;

        // Ajustez la position de la balle en fonction du mouvement de l'ennemi
        //tabBalle[i].posBalle.posX -= vitesse * ((SDL_GetTicks() - tempsDernierTir) / 1000.0);
    }
}






