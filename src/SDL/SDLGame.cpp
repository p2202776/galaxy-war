
#include "SDLGame.h"
#include "iostream"

#define height 426
#define width 640

void erreur(){

    std::cerr << "Erreur : " << SDL_GetError() << std::endl;
    SDL_Quit() ;
}

 SDLGame::SDLGame():game()/*initialise le game*/ { // Initialisation de SDL
     if (SDL_Init(SDL_INIT_VIDEO) < 0) {
         erreur();
     }

     // Création de la fenêtre
     fenetre = SDL_CreateWindow("Ma Fenetre SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,
                                SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE);
     if (fenetre == nullptr) {
         erreur();
     }

     //creation du rendu
     renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED);
     if (renderer == nullptr) {
         SDL_DestroyWindow(fenetre);
         erreur();
     }
     //TODO SDL trouve l*image en comptatnt depuis l'executable

      //la on initialise toutes les donnees membres SDLGame.h
     //sprite.loidTexture(renderer,"../data/sample.bmp");
     spriteEnnemi.loidTexture(renderer,"../data/sample.bmp");
     //sprite1.loidTexture(renderer,"../data/sample.bmp");
     balleSprite.loidTexture(renderer,"../data/sample.bmp");
     spriteAvion.loidTexture(renderer,"../data/sample.bmp");
     //game.initTabEnnemi();



 }


SDLGame::~SDLGame() {}


void SDLGame::sdlDraw() {
    SDL_RenderClear(renderer);
    //sprite.drawSprite(renderer,150,250,25,20);
    //spriteEnnemi.drawSprite(renderer,game.e.pos.posX,game.e.pos.posY,25,20);

    spriteAvion.drawSprite(renderer,game.avion.getPosX(),game.avion.getPosY(),avionWidth,avionHeigh);
    initTabEnnemiTexture(game.tabEnnemi);
    initBalleTexture();
}

void SDLGame::update() {
    //game.e.bougerHorizontal() ;
    bougerTabEnnemi(game.tabEnnemi);

    for(int i=0 ;i<4;i++){
        game.tabEnnemi[i].tirerBalle(temps());
    }
    for(int i=0 ;i<4;i++){
        game.tabEnnemi[i].deplacerBalle();
    }
    game.collisionAvionBalle();
    game.gameOver();




}



void SDLGame::initTabEnnemiTexture(Ennemi tabEnnemi[]) {
    for(int i=0 ;i<4;i++){
        spriteEnnemi.drawSprite(renderer,tabEnnemi[i].pos.posX,tabEnnemi[i].pos.posY,ennemiWidth,ennemiHeigh);
    }
}

void SDLGame::initBalleTexture() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < game.tabEnnemi[i].tabBalle.size(); j++) {
            balleSprite.drawSprite(renderer,game.tabEnnemi[i].tabBalle[j].posBalle.posX,game.tabEnnemi[i].tabBalle[j].posBalle.posY,10,15);
        }
    }
}


void SDLGame::bougerTabEnnemi(Ennemi tabEnnemi[]) {
    for(int i=0 ;i<4;i++){
        tabEnnemi[i].bougerHorizontal();
    }
}

void SDLGame::Renderer(){
   // SDL_RenderClear(renderer);

}


void SDLGame::runProject() {

    // Attendre que l'utilisateur ferme la fenêtre
    bool isOpen = true;
    SDL_Event event;
    while (isOpen) {
        while (SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                case SDL_QUIT:
                    isOpen = false;
                    break;
                    //juste pour la gestion du deplacement de l'avion
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_LEFT:
                            game.gestionClavier('l');
                            break;
                        case SDLK_RIGHT:
                            game.gestionClavier('r');
                            break;
                        case SDLK_UP:
                            game.gestionClavier('u');
                            break;
                        case SDLK_DOWN:
                            game.gestionClavier('d');
                            break;

                    }
                    break;
                     // Ajoutez d'autres cas pour gérer d'autres événements si nécessaire
            }
        }

       // game.e.bougerHorizontal() ;


        update();
        sdlDraw();
        SDL_RenderPresent(renderer) ;
        SDL_Delay(20);
        //Renderer();
    }
}



