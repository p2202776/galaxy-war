
#include "jeuTxt.h"
#include "winTxt.h"
#include "../core/game.h"

#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // WIN32


void afficher(WinTXT & win, const Game & G)
{

   win.clear();

    // Affichage de la map
    for(int i=0;i<G.M.dimX;i++)
    {
        for(int j=0; j<G.M.dimY;j++)
        {
            win.print(i,j,G.M.getObjetChar(i,j));
        }
    }
    for(int i=0;i<4;i++)
    {
        win.print(G.tabEnnemi[i].pos.posX,G.tabEnnemi[i].pos.posY,'E');
    }

    // Affichage de l'avion
    win.print(G.avion.getPosX(),G.avion.getPosY(),'A');
    for(int i=0; i<G.avion.getTab().size();i++)
    {
        win.print(G.avion.getTab()[i].posBalle.posX,G.avion.getTab()[i].posBalle.posY,'b');
    }
    win.draw();
}

void boucleJeuTxt(Game & G)
{
    WinTXT win(G.M.dimX,G.M.dimY);
    bool jeu = true;
    int c;
    do
    {
        termClear();
        afficher(win,G);
    #ifdef _WIN32
        Sleep(100);
    #else
        usleep(100000);
    #endif // WIN32
        const Point collision ={G.M.dimX-1,G.M.dimY-1};
        c = win.getCh();
        G.avion.misAjourBalle();
        for(int i =0; i<4;i++)
        {
            G.tabEnnemi[i].bougerHorizontal();

        }
        switch (c) {
            case 'h':
                G.gestionClavier('u',collision);
                break;
            case 'b':
                G.gestionClavier('d',collision);
                break;
            case 'g':
                G.gestionClavier('l',collision);
                break;
            case 'd':
                G.gestionClavier('r',collision);
                break;
            case 'x': G.gestionClavier('x',collision);
                break;
            case 'q':
                jeu=false;
                break;
            default:
                break;

        }
    }while(jeu);

}




