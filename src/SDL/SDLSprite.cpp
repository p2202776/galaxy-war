#include "SDLSprite.h"



SDLSprite::SDLSprite(){
    surface = nullptr;
    texture = nullptr ;

}

SDLSprite::~SDLSprite(){
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);

}

void SDLSprite::loidTexture(SDL_Renderer *renderer,char* path) {
    surface = SDL_LoadBMP(path);
    if (surface == nullptr) {
        std::cerr << "Erreur lors du chargement de l'image BMP : " << SDL_GetError() << std::endl;
        return;
    }

    // Créer une texture à partir de la surface
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (texture == nullptr) {
        std::cerr << "Erreur lors de la création de la texture : " << SDL_GetError() << std::endl;
        SDL_FreeSurface(surface);
        return;
    }
}


void SDLSprite::drawSprite(SDL_Renderer *renderer,int x ,int y,int w,int h){
    if(texture==nullptr)
        std::cout<<"la texture est vide"<<SDL_GetError()<<std::endl ;

    SDL_Rect destRect={x,y,w,h};
    SDL_RenderCopy(renderer, texture ,NULL,&destRect);

}